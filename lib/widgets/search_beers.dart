

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:world_beers/controllers/beers_controller.dart';
import 'package:world_beers/models/beers_model.dart';

class BeersSearchDelegate extends SearchDelegate {

  @override
  String get searchFieldLabel => 'Nome';

  @override
  List<Widget> buildActions(BuildContext context) {
      return [
        IconButton(
          icon: Icon( Icons.clear ),
          onPressed: () => query = '',
        )
      ];
  }
  
    @override
    Widget buildLeading(BuildContext context) {
      return IconButton(
        icon: Icon( Icons.arrow_back ),
        onPressed: () => close(context, null ),
      );
    }
  
    @override
    Widget buildResults(BuildContext context) {
      
      return Text('buildResults');
    }
  
    @override
    Widget buildSuggestions(BuildContext context) {
    
      if( query.isEmpty ) {
        return _EmptyContainer();
      }

      final beerProvider = Provider.of<BeersController>(context, listen: false);
      beerProvider.getSuggestionsByQuery( query );


      return StreamBuilder(
        stream: beerProvider.suggestionStream,
        builder: ( _, AsyncSnapshot<List<Beer>> snapshot) {
          
          if( !snapshot.hasData ) return _EmptyContainer();

          final beers = snapshot.data!;

          return ListView.builder(
            itemCount: beers.length,
            itemBuilder: ( _, int index ) => _BeerItem( beers[index])
          );
        },
      );

  }

}


class _BeerItem extends StatelessWidget {

  final Beer beer;

  const _BeerItem( this.beer );

  @override
  Widget build(BuildContext context) {

    return ListTile(
      leading: Image(
        image: NetworkImage( beer.avatar ),
        width: 50,
        fit: BoxFit.contain,
      ),
      title: Text( beer.name ),
      subtitle: Text( beer.description ),
      onTap: () {
        
      },
    );
  }
}
class _EmptyContainer extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 95,
        child: Image(image:AssetImage('assets/beer-icon.png'))
      ),
    );
  }
}