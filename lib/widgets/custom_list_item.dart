import 'package:flutter/material.dart';

class CustomListItem extends StatelessWidget {
  const CustomListItem({
    Key? key,
    required this.thumbnail,
    required this.name,
    required this.description,
    required this.avc,
    required this.ibu
  }) : super(key: key);

  final Widget thumbnail;
  final String name;
  final String description;
  final String avc;
  final String ibu;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 3.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: thumbnail,
          ),
          Expanded(
            flex: 3,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Text(
                    name,
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 14.0,
                    ),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 5, 20, 5),
                  child: Text(
                    description,
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.justify,
                    style: TextStyle(fontSize: 12.0, color: Colors.grey[800]),
                  ),
                ),

                _Measures(measure: avc, image: 'assets/degrees.png'),

                _Measures(measure: ibu, image: 'assets/bitter.png'),

              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _Measures extends StatelessWidget {
  const _Measures({
    Key? key,
    required this.measure, 
    required this.image,
  }) : super(key: key);

  final String measure;
  final String image;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          height: 25,
          margin: EdgeInsets.only(right: 5.0),
          child: Image(image:AssetImage(image))
        ),
        Text(measure,
          style: const TextStyle(fontSize: 11.0),
        ),
      ],
    );
  }
}