import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:world_beers/controllers/beers_controller.dart';
import 'package:world_beers/views/details_screen.dart';
import 'package:world_beers/widgets/custom_list_item.dart';

class CustomCard extends StatefulWidget {
  @override
  _CustomCardState createState() => _CustomCardState();
}

class _CustomCardState extends State<CustomCard> {

  final ScrollController scrollController = new ScrollController();

  @override
  void initState() { 
    super.initState();
    final BeersController beersController = Provider.of<BeersController>(context, listen: false);
    scrollController.addListener(() {
      if ( scrollController.position.pixels >= scrollController.position.maxScrollExtent - 500) {
        beersController.loadBeers();
      }
    });
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    final beerProvider = Provider.of<BeersController>(context);
    
    return ListView.builder(
      controller: scrollController,
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      itemCount: beerProvider.beerResults.length,
      itemBuilder: (context, index) {
        return Hero(
          tag: beerProvider.beerResults[index].id,
          child: GestureDetector(
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => DetailsScreen(beerProvider.beerResults[index])),
            ),
            child: Card(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              elevation: 2,
              color: Colors.white,
              margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
              child: CustomListItem(
                name: beerProvider.beerResults[index].name,
                description: beerProvider.beerResults[index].description,
                avc: beerProvider.beerResults[index].getAvc,
                ibu: beerProvider.beerResults[index].getIbu,
                thumbnail: Container(
                  height: 110,
                  width: 10,
                  margin: EdgeInsets.all(5.0),
                  child: Image(image: NetworkImage(beerProvider.beerResults[index].avatar))
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}