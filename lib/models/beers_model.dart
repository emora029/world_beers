import 'dart:convert';

List<Beer> beerFromJson(String str) => List<Beer>.from(json.decode(str).map((x) => Beer.fromJson(x)));

class Beer {
    Beer({
      required this.id,
      required this.name,
      required this.description,
      required this.foodPairing,
      this.imageUrl,
      this.abv,
      this.ibu,
      this.firstBrewed,
      this.brewersTips,
    });

    int id;
    String name;
    String description;
    String? imageUrl;
    double? abv;
    double? ibu;
    String? firstBrewed;
    List<String> foodPairing;
    String? brewersTips;

    get avatar {
      if(this.imageUrl != null)
        return imageUrl;

      return 'https://st4.depositphotos.com/14953852/24787/v/600/depositphotos_247872612-stock-illustration-no-image-available-icon-vector.jpg';
    }

    get getAvc {
      if(this.abv != null)
        return abv.toString();

      return 'N/A';
    }

    get getIbu {
      if(this.ibu != null)
        return ibu.toString();

      return 'N/A';
    }

    factory Beer.fromJson(Map<String, dynamic> json) => Beer(
      id: json["id"],
      name: json["name"],
      description: json["description"],
      imageUrl: json["image_url"],
      abv: json["abv"].toDouble(),
      ibu: json["ibu"] == null ? null : json["ibu"].toDouble(),
      firstBrewed: json["first_brewed"],
      foodPairing: List<String>.from(json["food_pairing"].map((x) => x)),
      brewersTips: json["brewers_tips"],
  );
}
