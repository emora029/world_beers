import 'package:flutter/material.dart';
import 'package:world_beers/models/beers_model.dart';

class DetailsScreen extends StatelessWidget {

  final Beer beer;

  const DetailsScreen(this.beer);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(254, 254, 254, 1.0),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          centerTitle: true,
          title: Text(beer.name, style: TextStyle(color: Colors.black)),
          leading: InkWell(
            onTap: () => Navigator.pop(context),
            child: Icon(
              Icons.arrow_back_ios,
              color: Colors.yellow[800],
              size: 16.0,
          )),
        ),
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Center(
              child: Container(
                child: Image(image:AssetImage('assets/fondo.jpg'))
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Hero(
                    tag: beer.id,
                    child: Container(
                      height: 300,
                      width: 100,
                      margin: EdgeInsets.all(12.0),
                      child: Image(image: NetworkImage(beer.avatar)),
                    ),
                  ),
                ),
      
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: Center(child: Text('Beer details', style: TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold))),
                ),
      
                Container(
                  padding: EdgeInsets.only(right: 35.0, top: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      beer.firstBrewed == null ? Container() :
                      Title('First Brewed', beer.firstBrewed.toString()),

                      beer.firstBrewed == null  ? Container() :
                      Title('Brewers Tips', beer.firstBrewed.toString()),
                    ],
                  ),
                ),
      
                Title('Food Pairing:', beer.foodPairing.join('\n')),
      
                
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class Title extends StatelessWidget {

  final String title;
  final String value;

  const Title(this.title, this.value);
  
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(35, 5.0, 15.0, 5.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title, style: TextStyle(fontSize: 15.5, fontWeight: FontWeight.w600)),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: Text(value, style: TextStyle(fontSize: 14.5)),
          ),
        ],
      ),
    );
  }
}