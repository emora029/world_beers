import 'package:flutter/material.dart';
import 'package:world_beers/widgets/custom_card.dart';
import 'package:world_beers/widgets/search_beers.dart';

class HomeScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: AppBar(
          centerTitle: true,
          title: Text('World Beers', style: TextStyle(color: Colors.black),),
          backgroundColor: Colors.white,
          elevation: 0,
          actions: [
          IconButton(
            icon: Icon( Icons.search_outlined, color: Colors.yellow[800]),
            onPressed: () => showSearch(context: context, delegate: BeersSearchDelegate() ),
            )
          ],
        ),
      ),
      body: CustomCard(),
    );
  }
}