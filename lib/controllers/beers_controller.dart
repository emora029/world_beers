import 'dart:async';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:world_beers/helpers/debouncer.dart';
import 'package:world_beers/models/beers_model.dart';

class BeersController extends ChangeNotifier {

  BeersController() {
    this.loadBeers();
  }

  List<Beer> beerResults = [];
  List<Beer> onlyBeer = [];
  int _page = 0;

  final debouncer = Debouncer(duration: Duration( milliseconds: 500 ));
  final StreamController<List<Beer>> _suggestionStreamContoller = new StreamController<List<Beer>>.broadcast();
  Stream<List<Beer>> get suggestionStream => this._suggestionStreamContoller.stream;
  
  loadBeers() async {
    _page++;
    
    var url = Uri.parse('https://api.punkapi.com/v2/beers?page=$_page');

    final response = await http.get(url);
    final List<Beer> nowBeers = beerFromJson(response.body);
    this.beerResults = [ ...beerResults, ...nowBeers ];
    notifyListeners();
  }

  Future<List<Beer>> searchBeers(String name) async {
    
    var url = Uri.parse('https://api.punkapi.com/v2/beers?beer_name=$name');

    final response = await http.get(url);
    final List<Beer> searchResponse = beerFromJson(response.body);
    return searchResponse;
  }

  void getSuggestionsByQuery(String busqueda) {

    debouncer.value = '';
    debouncer.onValue = ( value ) async {
      final resultados = await this.searchBeers(value);
      this._suggestionStreamContoller.add(resultados);
    };

    final timer = Timer.periodic(Duration(milliseconds: 200), (_) {
      debouncer.value = busqueda;
    });

    Future.delayed(Duration(milliseconds: 201)).then((_) => timer.cancel()); 

  }
  
}